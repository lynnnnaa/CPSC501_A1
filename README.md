## CPSC501_A1

#CPSC 501 - Assignment 1 Report
*Jiarong Xu*
*30002042*
*Oct 9, 2020*
 

This python code inheritance.py calculates perimeter, area and volume of various shapes.
Source: https://github.com/chenmingxue/Python_PracticeProjects/blob/master/10_MultiInheritance.py


There were five refactoring listed as following:

A)
1) See figures in report

2) In inheritance.py, the fist print (line 27) was going to calculate the perimeter of a cube, but ends up getting the perimeter of a rectangle. 

3) I fixed the calculating mistake by defining one extra function under class ‘Cube’ called ‘perimeter’ calculating the perimeter of a cube. Then called the ‘perimeter’ function by ‘print(cube.perimeter)’ instead of print(cube.circumstance).

4) Indicted in Figure 1. Line 26 - 29, 31 was the result of the refactoring.

5) The updated code was tested successfully.

6) The previous code was done incorrectly.

7) No, this refactoring was complete.

8) This refactoring refers to the second commit called ‘fixed cube perimeter’ (SHA: 425f3e68ddd1a2f4e7209abc693e8218caeb9ec7) in branch ‘branch_1’




B)
1) See figures in report

2) In inheritance.py, line 27-28, 60-61 prints the results. However it doesn’t indicate which number refers to which result.

3) This issue was fixed by printing an instruction before each result number.

4) Indicted in Figure 2. Line 27 - 28, 60 - 61 was the result of the refactoring.

5) The updated code was tested successfully.

6) The previous code was not instructed well.

7) Yes, this brings to the next refactoring.

8) This refactoring refers to the second commit called ‘output info changed’ (SHA: 8e615c11b9365d5a80d13d25462bf4e8eabbbd57) in branch ‘main’. However it conflict with branch ‘branch_1’. Thus commit 'merged and fixed conflict’ (SHA: 42e61a7e1fbafbf4f8e9113282f40986b81466ff) merged branch ‘main’ and ‘branch_1’ and fixed the conflict.














C)
1) See figures in report

2) In inheritance.py, line 30-39, 67-70 prints the results. However it actually can be done in the unit test.

3) This issue was fixed by deleting all the print function.

4) Indicted in Figure 3. Line 30-39, 67-70 was the result of the refactoring.

5) The updated code was tested successfully.

6) The previous code was redundant.

7) No, this refactoring was complete.

8) This refactoring refers to the commit called ‘remove print’ (SHA: 8ec1d667c59e07b7b078a1c0f7a345e5148d76a9) in branch ‘main’.




D)
1) See figures in report

2) In inheritance.py, line 5 and 30, it was inefficient by defining a new triangle function.

3) This issue was fixed by making the triangle function an inheritance from the rectangle function, making rectangle the base function (grandpa function in this case).

4) Indicted in Figure 3. Line 5 and 30 was the result of the refactoring.

5) The updated code was tested successfully.

6) The code can be optimized by switching triangle to inheritance function.

7) No, this refactoring was complete.

8) This refactoring refers to the commit (SHA: 4465bb5151966a493105cf51dae383090283eff4) in branch ‘main’.
