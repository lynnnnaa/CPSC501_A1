class Rectangle():
    def __init__(self, length, width, **kwargs):
        self.length = length
        self.width = width

    def area(self):
        return self.length * self.width

    def circumstance(self):
        return (self.length + self.width) * 2

class Square(Rectangle):
    def __init__(self, length, **kwargs):
        super().__init__(length=length, width=length, **kwargs)

class Cube(Square):
    def surface_area(self):
        face_area = super().area()
        return face_area * 6

    def volume(self):
        face_area = super().area()
        return face_area * self.length

#The perimeter of a cube is 12 * a which a is the edge of the cube
    def perimeter(self):
        return (self.length + self.width) * 6

class Triangle(Rectangle):
    def __init__(self, base, height, **kwargs):
        self.base = base
        self.height = height
        super().__init__(**kwargs)

    def tri_area(self):
        return 0.5 * self.base * self.height


class Pyramid(Square, Triangle): #multiple super class, here **kwargs is used
    def __init__(self, base, slant_height, **kwargs):
        self.base = base
        self.slant_height = slant_height
        kwargs["height"] = slant_height
        kwargs["length"] = base
        super().__init__(base=base, **kwargs)

    def pyr_area1(self):
        base_area = super().area()
        perimeter = super().circumstance()
        return 0.5 * perimeter * self.slant_height + base_area

    def pyr_area2(self):
        base_area = super().area()
        triangle_area = super().tri_area()
        return triangle_area * 4 + base_area







import unittest

class testShape(unittest.TestCase):
    def area_of_pyrimid(self, base, slant_height):
        return base ** 2 + base * slant_height * 2

    def area_of_rectangle(self, base, height):
        return base * height

    # Rectangle Area Test
    def test1(self):
        base = 1
        height = 1
        self.assertEqual(Rectangle(base, height).area(), self.area_of_rectangle(base, height))

    def test2(self):
        base = 1.9
        height = 10.3
        self.assertEqual(Rectangle(base, height).area(), self.area_of_rectangle(base, height))

    # Pyramid Sanity Test
    def test3(self):
        base = 2
        height = 2
        self.assertEqual(Pyramid(base, height).pyr_area1(), Pyramid(base, height).pyr_area2())

    def test4(self):
        base = 10
        height = 12
        self.assertEqual(Pyramid(base, height).pyr_area1(), Pyramid(base, height).pyr_area2())

    def test5(self):
        base = 1.44
        height = 2.58
        self.assertEqual(Pyramid(base, height).pyr_area1(), Pyramid(base, height).pyr_area2())

    # Pyramid Value Test
    def test6(self):
        base = 2
        height = 2
        self.assertEqual(Pyramid(base, height).pyr_area1(), self.area_of_pyrimid(base, height))

    def test7(self):
        base = 10
        height = 12
        self.assertEqual(Pyramid(base, height).pyr_area1(), self.area_of_pyrimid(base, height))

    def test8(self):
        base = 1.44
        height = 2.58
        self.assertEqual(Pyramid(base, height).pyr_area1(), self.area_of_pyrimid(base, height))
